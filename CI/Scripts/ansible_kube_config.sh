#!/bin/bash

#Install Kubernetes remotely from the GitLab runner
export ANSIBLE_HOST_KEY_CHECKING=False
ansible-playbook -i ./TCAWSKubeDeploy/kube_hosts_data ./TCAWSKubeDeploy/kube_playbooks/kube_dependencies.yml --extra-vars "@versions.json"
ansible-playbook -i ./TCAWSKubeDeploy/kube_hosts_data ./TCAWSKubeDeploy/kube_playbooks/kube_master.yml
ansible-playbook -i ./TCAWSKubeDeploy/kube_hosts_data ./TCAWSKubeDeploy/kube_playbooks/kube_workers_connect.yml
